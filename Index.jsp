<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
%><!doctype html>
<html><head>
<meta charset="UTF-8">
<title>Сервер видеоконференций Enterego</title>
<link href="main.css" rel="stylesheet" />
</head><body>

<div><div>

<%@ include file="bbb_api.jsp"%>

<%
// ------------------------------------------------------------------------------- //

HashMap<String, HashMap> allMeetings = new HashMap<String, HashMap>();
HashMap<String, String> meeting;

String welcome = "<br>Вы участвуете в видеоконференции <b>%%CONFNAME%%</b>!<br><br>Наш официальный сайт <a href=\"event:https://www.enterego.ru\"><u>www.enterego.ru </u></a>.<br><br> <b>Для того, чтобы позвонить в конференцию, наберите <b>+7(495)255-15-65</b> д.н. <b>%%CONFNAME%%</b>.</b>";

meeting = new HashMap<String, String>();
allMeetings.put("2000", meeting);
	meeting.put("welcomeMsg", 	welcome);
	meeting.put("moderatorPW", 	"0000");
	meeting.put("viewerPW", 	"0000");
	meeting.put("voiceBridge", 	"72013");
	meeting.put("logoutURL", 	"/index.jsp");


meeting = new HashMap<String, String>();
allMeetings.put("2001", meeting);
	meeting.put("welcomeMsg", 	welcome);
	meeting.put("moderatorPW", 	"0000");
	meeting.put("viewerPW", 	"0000");
	meeting.put("voiceBridge", 	"72213");
	meeting.put("logoutURL", 	"/index.jsp");

meeting = new HashMap<String, String>();
allMeetings.put("2002", meeting);
        meeting.put("welcomeMsg",       welcome);
        meeting.put("moderatorPW",      "0000");
        meeting.put("viewerPW",         "0000");
        meeting.put("voiceBridge",      "72023");
        meeting.put("logoutURL",        "/index.jsp");

meeting = null;

Iterator<String> meetingIterator = new TreeSet<String>(allMeetings.keySet()).iterator();

// ------------------------------------------------------------------------------- //
if (request.getParameterMap().isEmpty()) {
// ------------------------------------------------------------------------------- //
%>

<h2>Зарегистрируйтесь и выберите конференцию для участия</h2><form method="get">
<table>
<tbody>
<tr>
	<td>Имя:</td>
	<td><input type="text" autofocus required name="username" /></td>
</tr>
<tr>
        <td>Конференция:</td>
        <td><select name="meetingID"><%
String key;
while (meetingIterator.hasNext()) {
        key = meetingIterator.next();
        out.println("<option value=\"" + key + "\">" + key + "</option>");
}
%></select></td>

<tr>
	<td>Пароль:</td>
	<td><input type="password" required name="password" /></td>
</tr>
</tr><tr>
	<td> </td>
	<td><label style="visibility:hidden;"><input type="checkbox" checked name="record" id="record" /></label>
	       
	<input type="submit" value="Войти" /></td>
</tr>
</tbody>
</table>
<input type="hidden" name="action" value="create" />
</form>

<%
// ------------------------------------------------------------------------------- //
} else if (request.getParameter("action").equals("create")) {
// ------------------------------------------------------------------------------- //

	String username = request.getParameter("username");
	String meetingID = request.getParameter("meetingID");
	String password = request.getParameter("password");
	String record = request.getParameter("record");

	meeting = allMeetings.get( meetingID );

	String welcomeMsg = meeting.get( "welcomeMsg" );
	String logoutURL = meeting.get( "logoutURL" );
	Integer voiceBridge = Integer.parseInt( meeting.get( "voiceBridge" ).trim() );

	String viewerPW = meeting.get( "viewerPW" );
	String moderatorPW = meeting.get( "moderatorPW" );

	// Check if we have a valid password

	if ( ! password.equals(viewerPW) && ! password.equals(moderatorPW) ) {
%><div class="error">Ошибка ввода пароля! Пожалуйста, <a href="javascript:history.go(-1)">попробуйте еще раз</a>.</div><%
		return;
	}
	// Looks good, let's create the meeting

	String meeting_ID = createMeeting( meetingID, welcomeMsg, moderatorPW, "OK", viewerPW, voiceBridge, logoutURL, record );

	// Check if we have an error.

	if( meeting_ID.startsWith("Error ") ) {
%><div class="error">Error: createMeeting() failed<br><%=meeting_ID%></div><%
		return;
	}

String joinURL = getJoinMeetingURL(username, meeting_ID, password, null);

%><script> window.location.href="<%=joinURL%>"; </script><%

// ------------------------------------------------------------------------------- //
}
// ------------------------------------------------------------------------------- //
%>

</div></div>

</body></html>

